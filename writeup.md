# **Traffic Sign Recognition** 

## Dataset Summary & Exploration

I used numpy arrays' methods to calculate summary statistics of the traffic
signs data set, as pickled data was of `np.array` type:

* name: train #examples: 34799
* name: valid #examples: 4410
* name: test #examples: 12630
* Image shape: (32, 32)
* #Unique classes: 43

## Dataset visualisation

Please, refer to `traffic_sign_classifier.ipynb/html`.
* Section:Show examples from datasets
* Section:Distribution of examples
* Section:Distribution of values

## Image normalization

I've used RGB images to not loose information from already small images.
All images have been image-wise normalized. See cell `[3]`.
```python
def load_data(name):
  DATASETS_DIR = 'datasets'
  path = os.path.join(DATASETS_DIR, name + '.p')
  with open(path, 'rb') as f:
    data = pickle.load(f)
    mean = np.mean(data['features'], axis=(1,2,3))
    stddev = np.std(data['features'], axis=(1,2,3))
    ins = []
    for i, m, s in zip(data['features'], mean, stddev):
  in_ = (i - m) / s
  ins.append(in_)
  data['features_normalized'] = np.array(ins)
  data['examples_n'] = data['features'].shape[0]
  return data
 ```

I didn't generate additional data.


## Network architecture

- input: 32x32x3
- conv2d: convolution 2D (3x3x3x32) -> relu -> maxpool -> dropout, output=15x15x32
- conv2d: convolution 2D (3x3x32x128) -> relu -> maxpool -> dropout, output=6x6x128
- conv2d: convolution 2D (3x3x128x256) -> relu -> maxpool -> dropout, output=2x2x256
- flaten: output=1024
- fc: W=1024x2048, output=2048
- fc: W=2048x256, output=256
- fc: W=256,43, output=43 `[logits]`
- softmax

## Training

I've used ADAM optimizer with learning rate `0.0001` and batch size `32`.
I've set up limits of epochs to `400` but I've implemented early stopping.
So, as soon as the network reached a certain accuracy on the validation set (in this case it was `0.97`) the pipline stoped and saved the network.

At the begining of each epochs examples from training set were suffled and then
the network were fed with examples batch by batch.

## Results

My final model results were:
* validation set accuracy: `0.97`
* test set accuracy of: `0.96`

I've found a suitable architecure iteratively.
- I've build a skeleton (3 x convolutions, 3xFC layers).
- Trained first 20 epochs
- Initially increated silightly number of layers
- After that, I've increated batch size and decreased LR to stabilize learning
  process

The architecture is similar to standard LeNet arch. Traffic images from this
data set are small and there are number of classes to predicts is also small, so
there is no need to build a more complicated model. Also, the network wokrs very
well on provided datasets. It quickly reaches above `0.97` accuracy on validation
dataset as well it's result on unseen images is also good (`0.96`).

## Test a Model on New Images

Here are German traffic signs that I found on the web:

![alt text](./signs-32x32/1.png)
![alt text](./signs-32x32/2.png)
![alt text](./signs-32x32/3.png)
![alt text](./signs-32x32/4.png)
![alt text](./signs-32x32/5.png)
![alt text](./signs-32x32/6.png)
![alt text](./signs-32x32/7.png)

The images seem to be pretty well readable and the network shouldn't have any
problems with classification. However, their colors distribution is completely
different. See notebook, section `Test network on the new examples`.
It causes that some examples has been classified incorrectly.

## New examples results

The model's accuracy (TOP1) on the new examples was `0.42`.

The model's accuracy (TOP5) on the new examples was `0.57`.

The model's accuracy (TOP1) on the test dataset was `0.96`.

Results on images downloaded from the web differs significantly, as values
distributions are completely different.

The model could be more robust if augumented datasets were used. I.e. adding
noise and manipulating slightly image values distribution. Also, I could try to
renormalize new images to match training image distributions.


## TOP5 predictions

The code is in cell `[35]`.
The results are as follows:

![alt text](./signs-32x32/1.png)
TOP5 for image: 1.png
- 1.00: [38] Keep right
- 0.00: [39] Keep left
- 0.00: [40] Roundabout mandatory
- 0.00: [20] Dangerous curve to the right
- 0.00: [37] Go straight or left

![alt text](./signs-32x32/6.png)
TOP5 for image: 6.png
- 0.21: [05] Speed limit (80km/h)
- 0.19: [16] Vehicles over 3.5 metric tons prohibited
- 0.12: [02] Speed limit (50km/h)
- 0.09: [11] Right-of-way at the next intersection
- 0.08: [07] Speed limit (100km/h)

![alt text](./signs-32x32/2.png)
TOP5 for image: 2.png
- 1.00: [01] Speed limit (30km/h)
- 0.00: [02] Speed limit (50km/h)
- 0.00: [00] Speed limit (20km/h)
- 0.00: [25] Road work
- 0.00: [05] Speed limit (80km/h)

![alt text](./signs-32x32/5.png)
TOP5 for image: 5.png
- 0.99: [16] Vehicles over 3.5 metric tons prohibited
- 0.00: [23] Slippery road
- 0.00: [09] No passing
- 0.00: [41] End of no passing
- 0.00: [10] No passing for vehicles over 3.5 metric tons

![alt text](./signs-32x32/3.png)
TOP5 for image: 3.png
- 0.62: [04] Speed limit (70km/h)
- 0.06: [09] No passing
- 0.06: [10] No passing for vehicles over 3.5 metric tons
- 0.05: [19] Dangerous curve to the left
- 0.05: [40] Roundabout mandatory

![alt text](./signs-32x32/7.png)
TOP5 for image: 7.png
- 1.00: [13] Yield
- 0.00: [15] No vehicles
- 0.00: [35] Ahead only
- 0.00: [01] Speed limit (30km/h)
- 0.00: [03] Speed limit (60km/h)

![alt text](./signs-32x32/4.png)
TOP5 for image: 4.png
- 1.00: [18] General caution
- 0.00: [26] Traffic signals
- 0.00: [24] Road narrows on the right
- 0.00: [27] Pedestrians
- 0.00: [22] Bumpy road
